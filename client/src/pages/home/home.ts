import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http, Headers } from '@angular/http'
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	public data:any;

  constructor(public navCtrl: NavController, public http: Http) 
  {
  	let header = new Headers();
  	header.append('Accept', 'application/json');
  	header.append('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjIzNmFkMTgxOTJlMzUzODM4YzgzZDVmZWVmYzZhNGY3YWQwYjMzYzEwNjNjODJhNDkwYmEwMDFmYmU5M2I1MWIwZDQ3NTEzYTk1YTYxYzI4In0.eyJhdWQiOiIxIiwianRpIjoiMjM2YWQxODE5MmUzNTM4MzhjODNkNWZlZWZjNmE0ZjdhZDBiMzNjMTA2M2M4MmE0OTBiYTAwMWZiZTkzYjUxYjBkNDc1MTNhOTVhNjFjMjgiLCJpYXQiOjE0OTI0OTQ1OTgsIm5iZiI6MTQ5MjQ5NDU5OCwiZXhwIjoxNTI0MDMwNTk4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.XExZlf-Xn9-KPKxqxnSA3sebQthukDd-GHfAkNZ5e3lVQkjRpG53FcVL6q73v5gUIPb3Cn9p3H6VV8j_k59-YB4y3UxaEPxxZNWgXlABRmpEwisFWg_CeiYqj0BUGNH1BWSqSXVEHz3obxwgrI7RzOHNjG0Rqnr62_TctzNZ7VlhfDS6CBZB1DAY62T6meIqyJIWlGHX1waZgEtVdfYg6BtTjM06fo53o5BbhtdIdRcaaVqqQIPShDHKmrtfflhqCVVqInFSYQMXuJEEh1SKZ2TnOd0jXR7FqytpiX7I9OwTxARU0rztoFKN-FaD7A5a2KGDSUjPA2yMao1e1hkXGGhCT2nMnC7BefGPeXFPKvVTJiJm27EgTKeQH0PpE5gvz_cJapyeOm5FKKzvkX2NaLSCX6KNVEb8-0Th7iwcm32cKcv3Tld2oZgJlyT1d8DDHGOgi89elQ81cXKEqv-csufZ9yBgnRJNVszq2fHGLhl9OsaNObn4BAPH87VJ6zpBjMk4tmawiWQQoGFVHwwhJOvAptDtJXiaYY-QJcPU6aHXI6bcv0p9jr4CSbx7Pqfa4Nb2kYvIIWVndvVj_cxjwTtHOhviXtNzurZROO0NV2D4f78ZXNI2Yf9e3WJQu_CJiR7KfCUkWKMqDmrhsVj8SATC4qdOFRoQpXSBra3ohG4')


  	this.http.get('http://localhost:8000/api/liquen', {headers: header})
  	.map(res => res.json())
  	.subscribe(
  		data => {
  			console.log(data);
  			this.data = data;
  		},
  		err => {
  			console.log('error')
  		}
  		);
  }

}
